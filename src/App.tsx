import { useState, useEffect } from 'react'

import Main from './components/Main'
import Navbar from './components/Navbar'

function App() {
  const [theme, setTheme] = useState(
    localStorage.getItem('theme') ||
      (window.matchMedia('(prefers-color-scheme: light)').matches
        ? 'light'
        : 'dark'),
  )

  useEffect(() => {
    window
      .matchMedia('(prefers-color-scheme: light)')
      .addEventListener('change', (event) => {
        const systemScheme = event.matches ? 'light' : 'dark'
        setTheme(systemScheme)
      })
  }, [])

  useEffect(() => {
    localStorage.setItem('theme', theme)
    document.body.className = `${theme}-theme`
  }, [theme])

  function handleToggle(event) {
    setTheme(event.target.checked ? 'dark' : 'light')
  }

  return (
    <div>
      <Navbar theme={theme} setTheme={setTheme} handleToggle={handleToggle} />
      <Main />
    </div>
  )
}

export default App
