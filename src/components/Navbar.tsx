import reactLogoLight from '../images/react-icon-small-light.png'
import reactLogoDark from '../images/react-icon-small-dark.png'

function Navbar(props) {
  return (
    <nav className='nav'>
      <img
        src={props.theme === 'light' ? reactLogoLight : reactLogoDark}
        className='nav--icon'
      />
      <h3 className='nav--logo_text'>ReactFacts</h3>
      <div className='theme-toggle'>
        <label htmlFor='toggle-switch' className='toggle-text light-text'>
          Light
        </label>
        <input
          onChange={props.handleToggle}
          type='checkbox'
          id='toggle-switch'
          className='toggle-switch'
          checked={props.theme === 'dark'}
        />

        <label htmlFor='toggle-switch' className='toggle-text dark-text'>
          Dark
        </label>
      </div>
    </nav>
  )
}

export default Navbar
